import { Libsodium } from "nativescript-libsodium";
import { TextDecoder } from "text-encoding";

describe("libsodium nativescript wrapper", function() {
    it("can generate a keypair", function() {
        let keypair = Libsodium.crypto_box_keypair();
        expect(keypair.privateKey).toBeDefined();
        expect(keypair.publicKey).toBeDefined();
    });

    it("can run crypto_pwhash", function() {
        const OPSLIMIT = 4;
        const MEMLIMIT = 33554432;
        const ALG = 1;

        let salt = new Uint8Array([ 88, 240, 185, 66, 195, 101, 160, 138, 137, 78, 1, 2, 3, 4, 5, 6]);
        let hash = Libsodium.crypto_pwhash(
            Libsodium.crypto_box_SEEDBYTES,
            "hunter2",
            salt,
            OPSLIMIT,
            MEMLIMIT,
            ALG,
        );
        expect(hash[0]).toEqual(49);
    });

    it("can run crypto_secretbox_easy and crypto_secretbox_open_easy", function() {
        let secretMessage = 'hello';
        let nonce = Libsodium.randombytes_buf(Libsodium.crypto_secretbox_NONCEBYTES);
        let key = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5]);
        let ciphertext = Libsodium.crypto_secretbox_easy(secretMessage, nonce, key);
        let decrypted = Libsodium.crypto_secretbox_open_easy(ciphertext, nonce, key);
        let decryptedString = new TextDecoder('utf-8').decode(decrypted);
        expect(decryptedString).toEqual(secretMessage);
    });

    it("can run crypto_box_seal and crypto_box_seal_open", function() {
        let secretMessage = 'hello';
        let keypair = Libsodium.crypto_box_keypair();
        let ciphertext = Libsodium.crypto_box_seal(secretMessage, keypair.publicKey);
        let decrypted = Libsodium.crypto_box_seal_open(ciphertext, keypair.publicKey, keypair.privateKey);
        let decryptedString = new TextDecoder('utf-8').decode(decrypted);
        expect(decryptedString).toEqual(secretMessage);
    });

    it("fakes the libsodium.js .ready function", function(done) {
        const result = Libsodium.ready.then(() => done());
        expect(result).toBeTruthy();
    });
});
