import * as observable from 'tns-core-modules/data/observable';
import * as pages from 'tns-core-modules/ui/page';
import { Libsodium } from 'nativescript-libsodium';

const message = Libsodium.DEMO_MSG();

// Event handler for Page 'loaded' event attached in main-page.xml
export function pageLoaded(args: observable.EventData) {
    // Get the event sender
    let page = <pages.Page>args.object;
    let context = {
        message: message,
    };
    page.bindingContext = context;
}