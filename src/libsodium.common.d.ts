export interface IKeyPair {
    keyType?: string;
    privateKey: Uint8Array;
    publicKey: Uint8Array;
}
export declare class Common {
    static BASE64_FLAGS: number;
    static base64_variants: {
        ORIGINAL: number;
        ORIGINAL_NO_PADDING: number;
        URLSAFE: number;
        URLSAFE_NO_PADDING: number;
    };
    static ready: Promise<void>;
    private static javaByteArrayToUint8Array;
    private static Uint8ArrayToJavaByteArray;
    static to_base64(aBytes: Uint8Array, variant?: any): string;
    static from_base64(sBase64: string, variant?: any): Uint8Array;
    static randombytes_buf(length: number): Uint8Array;
    static crypto_box_keypair(): IKeyPair;
    static crypto_pwhash(keyLength: number, password: string | Uint8Array, salt: Uint8Array, opsLimit: number, memlimit: number, algorithm: number): Uint8Array;
    private static force_bytes;
    private static force_java_bytes;
    private static trimJavaBytes;
    static crypto_box_easy(message: string | Uint8Array, nonce: Uint8Array, publicKey: any, privateKey: any): Uint8Array;
    static crypto_box_open_easy(ciphertext: Uint8Array, nonce: Uint8Array, publicKey: Uint8Array, privateKey: Uint8Array): Uint8Array;
    static crypto_secretbox_easy(message: string | Uint8Array, nonce: Uint8Array, key: Uint8Array): Uint8Array;
    static crypto_secretbox_open_easy(ciphertext: Uint8Array, nonce: Uint8Array, key: Uint8Array): Uint8Array;
    static crypto_box_seal(message: string | Uint8Array, publicKey: Uint8Array): Uint8Array;
    static crypto_box_seal_open(ciphertext: Uint8Array, publicKey: Uint8Array, secretKey: Uint8Array): Uint8Array;
    static randombytes_uniform(upper_bound: number): number;
    static readonly crypto_aead_chacha20poly1305_ABYTES: number;
    static readonly crypto_aead_chacha20poly1305_KEYBYTES: number;
    static readonly crypto_aead_chacha20poly1305_NPUBBYTES: number;
    static readonly crypto_aead_chacha20poly1305_NSECBYTES: number;
    static readonly crypto_auth_BYTES: number;
    static readonly crypto_auth_KEYBYTES: number;
    static readonly crypto_auth_hmacsha256_BYTES: number;
    static readonly crypto_auth_hmacsha256_KEYBYTES: number;
    static readonly crypto_auth_hmacsha512_BYTES: number;
    static readonly crypto_auth_hmacsha512_KEYBYTES: number;
    static readonly crypto_box_BEFORENMBYTES: number;
    static readonly crypto_box_MACBYTES: number;
    static readonly crypto_box_NONCEBYTES: number;
    static readonly crypto_box_PUBLICKEYBYTES: number;
    static readonly crypto_box_SEALBYTES: number;
    static readonly crypto_box_SECRETKEYBYTES: number;
    static readonly crypto_box_SEEDBYTES: number;
    static readonly crypto_generichash_BYTES: number;
    static readonly crypto_generichash_BYTES_MAX: number;
    static readonly crypto_generichash_BYTES_MIN: number;
    static readonly crypto_generichash_KEYBYTES: number;
    static readonly crypto_generichash_KEYBYTES_MAX: number;
    static readonly crypto_generichash_KEYBYTES_MIN: number;
    static readonly crypto_onetimeauth_BYTES: number;
    static readonly crypto_onetimeauth_KEYBYTES: number;
    static readonly crypto_pwhash_ALG_ARGON2I13: number;
    static readonly crypto_pwhash_ALG_DEFAULT: number;
    static readonly crypto_pwhash_MEMLIMIT_INTERACTIVE: number;
    static readonly crypto_pwhash_MEMLIMIT_MODERATE: number;
    static readonly crypto_pwhash_MEMLIMIT_SENSITIVE: number;
    static readonly crypto_pwhash_OPSLIMIT_INTERACTIVE: number;
    static readonly crypto_pwhash_OPSLIMIT_MODERATE: number;
    static readonly crypto_pwhash_OPSLIMIT_SENSITIVE: number;
    static readonly crypto_pwhash_SALTBYTES: number;
    static readonly crypto_pwhash_STRBYTES: number;
    static readonly crypto_scalarmult_BYTES: number;
    static readonly crypto_scalarmult_SCALARBYTES: number;
    static readonly crypto_secretbox_KEYBYTES: number;
    static readonly crypto_secretbox_MACBYTES: number;
    static readonly crypto_secretbox_NONCEBYTES: number;
    static readonly crypto_shorthash_BYTES: number;
    static readonly crypto_shorthash_KEYBYTES: number;
    static readonly crypto_sign_BYTES: number;
    static readonly crypto_sign_PUBLICKEYBYTES: number;
    static readonly crypto_sign_SECRETKEYBYTES: number;
    static readonly crypto_sign_SEEDBYTES: number;
    static readonly crypto_stream_chacha20_KEYBYTES: number;
    static readonly crypto_stream_chacha20_NONCEBYTES: number;
    static DEMO_MSG(): string;
}
