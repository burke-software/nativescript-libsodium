import { Observable } from 'tns-core-modules/data/observable';
import * as app from 'tns-core-modules/application';
import * as dialogs from 'tns-core-modules/ui/dialogs';

import { TextEncoder, TextDecoder } from "text-encoding";

const Random = org.libsodium.jni.crypto.Random;
const SodiumConstants = org.libsodium.jni.SodiumConstants;
const NaCl = org.libsodium.jni.NaCl;
const Sodium = org.libsodium.jni.Sodium;
const Arrays = java.util.Arrays;

NaCl.sodium();

let Base64 = android.util.Base64;

type JavaBytes = native.Array<number>;

export interface IKeyPair {
  keyType?: string;
  privateKey: Uint8Array;
  publicKey: Uint8Array;
}

export class Common {
  static BASE64_FLAGS = Base64.NO_WRAP | Base64.NO_CLOSE;
  static base64_variants = {
    ORIGINAL: 1,
    ORIGINAL_NO_PADDING: 3,
    URLSAFE: 5,
    URLSAFE_NO_PADDING: 7
  };
  static ready = Promise.resolve();

  private static javaByteArrayToUint8Array(javaBytes: JavaBytes) {
    let out = new Uint8Array(javaBytes.length);
    for (let i = 0; i < javaBytes.length; i++) {
      out[i] = javaBytes[i];
    }
    return out;
  }

  private static Uint8ArrayToJavaByteArray(inBytes: Uint8Array): JavaBytes {
    let simpleArr = Array.from(inBytes);
    return simpleArr;
  }

  public static to_base64(aBytes: Uint8Array, variant?: any): string {
    let javaArr = this.Uint8ArrayToJavaByteArray(aBytes);
    let out = Base64.encodeToString(javaArr, this.BASE64_FLAGS);
    return out;
  }

  public static from_base64(sBase64: string, variant?: any): Uint8Array {
    let out = Base64.decode(sBase64, this.BASE64_FLAGS);
    return this.javaByteArrayToUint8Array(out);
  }

  public static randombytes_buf(length: number): Uint8Array {
    let rand = new Random().randomBytes(length);
    return this.javaByteArrayToUint8Array(rand);
  }

  public static crypto_box_keypair(): IKeyPair {
    let rand = new Random().randomBytes(SodiumConstants.SECRETKEY_BYTES);
    let keys = new org.libsodium.jni.keys.KeyPair(rand);
    let publicKey = keys.getPublicKey();
    let privateKey = keys.getPrivateKey();
    return {
      privateKey: this.javaByteArrayToUint8Array(privateKey.toBytes()),
      publicKey: this.javaByteArrayToUint8Array(publicKey.toBytes()),
      keyType: "curve25519"
    };
  }

  public static crypto_pwhash(keyLength: number, password: string | Uint8Array, salt: Uint8Array, opsLimit: number, memlimit: number, algorithm: number) {
    let passwd = this.force_bytes(password);
    let key: JavaBytes = Array.create('byte', keyLength);
    let passwordBytes: JavaBytes = this.Uint8ArrayToJavaByteArray(passwd);

    Sodium.crypto_pwhash(
      key,
      keyLength,
      passwordBytes,
      passwordBytes.length,
      this.Uint8ArrayToJavaByteArray(salt),
      opsLimit,
      memlimit,
      algorithm
    );

    let keyBytes = this.javaByteArrayToUint8Array(key);
    return keyBytes;
  }

  /** Ensure input is Uint8Array, if it's a string, assume it's utf-8 and convert */
  private static force_bytes(input: string | Uint8Array) {
    let inputBytes: Uint8Array;
    if (typeof(input) === "string") {
      inputBytes = new TextEncoder('utf-8').encode(input);
    } else {
      inputBytes = input;
    }
    return inputBytes;
  }

  /** Forces input as Java Byte Array */
  private static force_java_bytes(input: string | Uint8Array): JavaBytes {
    let jsBytes = this.force_bytes(input);
    return this.Uint8ArrayToJavaByteArray(jsBytes);
  }

  /** Remove excess 0 bytes from Java array. libsodium requires you to know the original message length
   * when decrypting. However libsodium.js doesn't force you to know this. So we just make it extra big and
   * trim the excess 0's. If you know a better way please let us know.
   */
  private static trimJavaBytes(input: JavaBytes): JavaBytes {
    let i = input.length - 1;
    while (i >= 0 && input[i] === 0) {
      --i;
    }
    let out = Arrays.copyOf(input as any, i + 1);
    return (out as any);
  }

  public static crypto_box_easy(
    message: string | Uint8Array,
    nonce: Uint8Array,
    publicKey: any,
    privateKey: any
  ): Uint8Array {
    let messageBytes = this.force_java_bytes(message);
    let nonceBytes = this.force_java_bytes(nonce);
    let publicKeyBytes = this.force_java_bytes(publicKey);
    let PrivateKeyBytes = this.force_java_bytes(privateKey);
    let ciphertext: JavaBytes = Array.create('byte', Sodium.crypto_box_macbytes() + messageBytes.length);

    Sodium.crypto_box_easy(ciphertext, messageBytes, messageBytes.length, nonceBytes, publicKeyBytes, PrivateKeyBytes);
    return this.javaByteArrayToUint8Array(ciphertext);
  }

  public static crypto_box_open_easy(
    ciphertext: Uint8Array,
    nonce: Uint8Array,
    publicKey: Uint8Array,
    privateKey: Uint8Array
  ): Uint8Array {
    let ciphertextBytes = this.force_java_bytes(ciphertext);
    let nonceBytes = this.force_java_bytes(nonce);
    let publicKeyBytes = this.force_java_bytes(publicKey);
    let privateKeyBytes = this.force_java_bytes(privateKey);
    let decrypted: JavaBytes = Array.create('byte', Sodium.crypto_box_macbytes() + ciphertextBytes.length);

    Sodium.crypto_box_open_easy(
      decrypted,
      ciphertextBytes,
      ciphertextBytes.length,
      nonceBytes,
      publicKeyBytes,
      privateKeyBytes,
    );

    decrypted = this.trimJavaBytes(decrypted);
    return this.javaByteArrayToUint8Array(decrypted);
  }

  public static crypto_secretbox_easy(message: string | Uint8Array, nonce: Uint8Array, key: Uint8Array): Uint8Array {
    let messageBytes = this.force_java_bytes(message);
    let nonceBytes = this.force_java_bytes(nonce);
    let keyBytes = this.force_java_bytes(key);
    let ciphertext: JavaBytes = Array.create('byte', Sodium.crypto_secretbox_macbytes() + messageBytes.length);

    Sodium.crypto_secretbox_easy(
      ciphertext,
      messageBytes,
      messageBytes.length,
      nonceBytes,
      keyBytes,
    );

    return this.javaByteArrayToUint8Array(ciphertext);
  }

  public static crypto_secretbox_open_easy(ciphertext: Uint8Array, nonce: Uint8Array, key: Uint8Array): Uint8Array {
    let ciphertextBytes = this.force_java_bytes(ciphertext);
    let nonceBytes = this.force_java_bytes(nonce);
    let keyBytes = this.force_java_bytes(key);
    let decrypted: JavaBytes = Array.create('byte', Sodium.crypto_secretbox_macbytes() + ciphertextBytes.length);

    Sodium.crypto_secretbox_open_easy(
      decrypted,
      ciphertextBytes,
      ciphertextBytes.length,
      nonceBytes,
      keyBytes,
    );

    decrypted = this.trimJavaBytes(decrypted);
    return this.javaByteArrayToUint8Array(decrypted);
  }

  public static crypto_box_seal(message: string | Uint8Array, publicKey: Uint8Array): Uint8Array {
    let messageBytes = this.force_java_bytes(message);
    let publicKeyBytes = this.force_java_bytes(publicKey);
    let ciphertext: JavaBytes = Array.create('byte', Sodium.crypto_box_sealbytes() + messageBytes.length);

    Sodium.crypto_box_seal(ciphertext, messageBytes, messageBytes.length, publicKeyBytes);

    return this.javaByteArrayToUint8Array(ciphertext);
  }

  public static crypto_box_seal_open(ciphertext: Uint8Array, publicKey: Uint8Array, secretKey: Uint8Array): Uint8Array {
    let ciphertextBytes = this.force_java_bytes(ciphertext);
    let publicKeyBytes = this.force_java_bytes(publicKey);
    let secretKeyBytes = this.force_java_bytes(secretKey);
    let decrypted: JavaBytes = Array.create('byte', Sodium.crypto_box_sealbytes() + ciphertextBytes.length);

    Sodium.crypto_box_seal_open(decrypted, ciphertextBytes, ciphertextBytes.length, publicKeyBytes, secretKeyBytes);

    decrypted = this.trimJavaBytes(decrypted);
    return this.javaByteArrayToUint8Array(decrypted);
  }

  public static randombytes_uniform(upper_bound: number) {
    return Sodium.randombytes_uniform(upper_bound);
  }

  public static get crypto_aead_chacha20poly1305_ABYTES() { return Sodium.crypto_aead_chacha20poly1305_abytes(); }
  public static get crypto_aead_chacha20poly1305_KEYBYTES() { return Sodium.crypto_aead_chacha20poly1305_keybytes(); }
  public static get crypto_aead_chacha20poly1305_NPUBBYTES() { return Sodium.crypto_aead_chacha20poly1305_npubbytes(); }
  public static get crypto_aead_chacha20poly1305_NSECBYTES() { return Sodium.crypto_aead_chacha20poly1305_nsecbytes(); }
  public static get crypto_auth_BYTES() { return Sodium.crypto_auth_bytes(); }
  public static get crypto_auth_KEYBYTES() { return Sodium.crypto_auth_keybytes(); }
  public static get crypto_auth_hmacsha256_BYTES() { return Sodium.crypto_auth_hmacsha256_bytes(); }
  public static get crypto_auth_hmacsha256_KEYBYTES() { return Sodium.crypto_auth_hmacsha256_keybytes(); }
  public static get crypto_auth_hmacsha512_BYTES() { return Sodium.crypto_auth_hmacsha512_bytes(); }
  public static get crypto_auth_hmacsha512_KEYBYTES() { return Sodium.crypto_auth_hmacsha512_keybytes(); }
  public static get crypto_box_BEFORENMBYTES() { return Sodium.crypto_box_beforenmbytes(); }
  public static get crypto_box_MACBYTES() { return Sodium.crypto_box_macbytes(); }
  public static get crypto_box_NONCEBYTES() { return Sodium.crypto_box_noncebytes(); }
  public static get crypto_box_PUBLICKEYBYTES() { return Sodium.crypto_box_publickeybytes(); }
  public static get crypto_box_SEALBYTES() { return Sodium.crypto_box_sealbytes(); }
  public static get crypto_box_SECRETKEYBYTES() { return Sodium.crypto_box_secretkeybytes(); }
  public static get crypto_box_SEEDBYTES() { return Sodium.crypto_box_seedbytes(); }
  public static get crypto_generichash_BYTES() { return Sodium.crypto_generichash_bytes(); }
  public static get crypto_generichash_BYTES_MAX() { return Sodium.crypto_generichash_bytes_max(); }
  public static get crypto_generichash_BYTES_MIN() { return Sodium.crypto_generichash_bytes_min(); }
  public static get crypto_generichash_KEYBYTES() { return Sodium.crypto_generichash_keybytes(); }
  public static get crypto_generichash_KEYBYTES_MAX() { return Sodium.crypto_generichash_keybytes_max(); }
  public static get crypto_generichash_KEYBYTES_MIN() { return Sodium.crypto_generichash_keybytes_min(); }
  public static get crypto_onetimeauth_BYTES() { return Sodium.crypto_onetimeauth_bytes(); }
  public static get crypto_onetimeauth_KEYBYTES() { return Sodium.crypto_onetimeauth_keybytes(); }
  public static get crypto_pwhash_ALG_ARGON2I13() { return Sodium.crypto_pwhash_alg_argon2i13(); }
  public static get crypto_pwhash_ALG_DEFAULT() { return Sodium.crypto_pwhash_alg_default(); }
  public static get crypto_pwhash_MEMLIMIT_INTERACTIVE() { return Sodium.crypto_pwhash_memlimit_interactive(); }
  public static get crypto_pwhash_MEMLIMIT_MODERATE() { return Sodium.crypto_pwhash_memlimit_moderate(); }
  public static get crypto_pwhash_MEMLIMIT_SENSITIVE() { return Sodium.crypto_pwhash_memlimit_sensitive(); }
  public static get crypto_pwhash_OPSLIMIT_INTERACTIVE() { return Sodium.crypto_pwhash_opslimit_interactive(); }
  public static get crypto_pwhash_OPSLIMIT_MODERATE() { return Sodium.crypto_pwhash_opslimit_moderate(); }
  public static get crypto_pwhash_OPSLIMIT_SENSITIVE() { return Sodium.crypto_pwhash_opslimit_sensitive(); }
  public static get crypto_pwhash_SALTBYTES() { return Sodium.crypto_pwhash_saltbytes(); }
  public static get crypto_pwhash_STRBYTES() { return Sodium.crypto_pwhash_strbytes(); }
  public static get crypto_scalarmult_BYTES() { return Sodium.crypto_scalarmult_bytes(); }
  public static get crypto_scalarmult_SCALARBYTES() { return Sodium.crypto_scalarmult_scalarbytes(); }
  public static get crypto_secretbox_KEYBYTES() { return Sodium.crypto_secretbox_keybytes(); }
  public static get crypto_secretbox_MACBYTES() { return Sodium.crypto_secretbox_macbytes(); }
  public static get crypto_secretbox_NONCEBYTES() { return Sodium.crypto_secretbox_noncebytes(); }
  public static get crypto_shorthash_BYTES() { return Sodium.crypto_shorthash_bytes(); }
  public static get crypto_shorthash_KEYBYTES() { return Sodium.crypto_shorthash_keybytes(); }
  public static get crypto_sign_BYTES() { return Sodium.crypto_sign_bytes(); }
  public static get crypto_sign_PUBLICKEYBYTES() { return Sodium.crypto_sign_publickeybytes(); }
  public static get crypto_sign_SECRETKEYBYTES() { return Sodium.crypto_sign_secretkeybytes(); }
  public static get crypto_sign_SEEDBYTES() { return Sodium.crypto_sign_seedbytes(); }
  public static get crypto_stream_chacha20_KEYBYTES() { return Sodium.crypto_stream_chacha20_keybytes(); }
  public static get crypto_stream_chacha20_NONCEBYTES() { return Sodium.crypto_stream_chacha20_noncebytes(); }


  public static DEMO_MSG(): string {
    let msg = '';

    let rand = this.randombytes_buf(org.libsodium.jni.SodiumConstants.SECRETKEY_BYTES);
    msg += `Some random bytes: ${rand}\n`;

    let keypair = this.crypto_box_keypair();
    let privateKey64 = this.to_base64(keypair.privateKey);
    msg += `Private key in base64: ${privateKey64}\n`;

    let opslimit = this.crypto_pwhash_OPSLIMIT_INTERACTIVE;
    msg += `The opslimit constant is ${opslimit}\n`;

    let passwd = "hunter2";
    let salt = new Uint8Array([ 88, 240, 185, 66, 195, 101, 160, 138, 137, 78, 1, 2, 3, 4, 5, 6]);

    let hash = this.crypto_pwhash(
      this.crypto_box_SEEDBYTES,
      passwd,
      salt,
      this.crypto_pwhash_OPSLIMIT_INTERACTIVE,
      this.crypto_pwhash_MEMLIMIT_INTERACTIVE,
      this.crypto_pwhash_ALG_DEFAULT,
    );
    msg += `The hash is ${hash}\n`;

    // let secretMessage = 'hello';
    // let nonce = this.randombytes_buf(this.crypto_secretbox_NONCEBYTES);
    // let key = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5]);
    // let ciphertext = this.crypto_secretbox_easy(secretMessage, nonce, key);
    // let decrypted = this.crypto_secretbox_open_easy(ciphertext, nonce, key);
    // let decryptedString = new TextDecoder('utf-8').decode(decrypted);
    let secretMessage = 'hello';
    let ciphertext = this.crypto_box_seal(secretMessage, keypair.publicKey);
    let decrypted = this.crypto_box_seal_open(ciphertext, keypair.publicKey, keypair.privateKey);
    let decryptedString = new TextDecoder('utf-8').decode(decrypted);

    msg += `decrypted is ${decryptedString}\n`;

    let randomNumber = this.randombytes_uniform(10);
    msg += `A random number between 0 and 10 is ${randomNumber}\n`;

    return msg;
  }
}
