declare namespace org {
    export namespace kaliumjni {
        export namespace lib {
            export class BuildConfig {
                public static DEBUG: boolean;
                public static APPLICATION_ID: string;
                public static BUILD_TYPE: string;
                public static FLAVOR: string;
                public static VERSION_CODE: number;
                public static VERSION_NAME: string;
                public constructor();
            }
        }
    }
}

/// <reference path="./org.libsodium.jni.Sodium.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export class NaCl {
                public static sodium(): org.libsodium.jni.Sodium;
            }
            export namespace NaCl {
                export class SingletonHolder {
                    public static SODIUM_INSTANCE: org.libsodium.jni.Sodium;
                }
            }
        }
    }
}

declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export class Sodium {
                public static crypto_generichash_blake2b_bytes_max(): number;
                public static randombytes(param0: native.Array<number>, param1: number): void;
                public static crypto_pwhash_opslimit_moderate(): number;
                public static crypto_secretbox_zerobytes(): number;
                public static crypto_hash_sha256_bytes(): number;
                public static crypto_secretbox_open_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_box_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_keybytes(): number;
                public static crypto_box_detached_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_auth_hmacsha256(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_auth_hmacsha512_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_stream_chacha20_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha512256_bytes(): number;
                public static crypto_generichash_keybytes(): number;
                public static crypto_generichash_blake2b_keybytes(): number;
                public static crypto_generichash_final(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_ed25519_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_sign_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_onetimeauth_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_generichash_blake2b_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_onetimeauth_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_pwhash_passwd_max(): number;
                public static crypto_pwhash_str(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number, param4: number): number;
                public static crypto_stream_xsalsa20_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_generichash_keybytes_min(): number;
                public static crypto_shorthash_siphash24_keybytes(): number;
                public static crypto_secretbox_macbytes(): number;
                public static crypto_secretbox_xsalsa20poly1305_noncebytes(): number;
                public static crypto_onetimeauth_primitive(): native.Array<number>;
                public static crypto_stream_salsa20_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_generichash_bytes_max(): number;
                public static crypto_auth_hmacsha256_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_boxzerobytes(): number;
                public static crypto_sign_ed25519_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_scalarmult_bytes(): number;
                public static crypto_scalarmult(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_box_open_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_pwhash_bytes_min(): number;
                public static crypto_onetimeauth_keybytes(): number;
                public static crypto_generichash_blake2b_personalbytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_open_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_pwhash_strbytes(): number;
                public static crypto_stream_xsalsa20_noncebytes(): number;
                public static crypto_box_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_macbytes(): number;
                public static crypto_pwhash_memlimit_min(): number;
                public static crypto_box_curve25519xsalsa20poly1305_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_stream_chacha20_ietf(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_shorthash_primitive(): native.Array<number>;
                public static crypto_pwhash_opslimit_min(): number;
                public static crypto_stream_salsa20(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_aead_chacha20poly1305_npubbytes(): number;
                public static crypto_onetimeauth_poly1305(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_seal_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_sign_ed25519_open(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_hash_sha256_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_secretbox_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_stream_salsa20_keybytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_stream_xsalsa20_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_boxzerobytes(): number;
                public static crypto_stream_chacha20_ietf_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_beforenm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_macbytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_zerobytes(): number;
                public static crypto_box_open_detached_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_pwhash_strprefix(): native.Array<number>;
                public static crypto_onetimeauth_statebytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_beforenmbytes(): number;
                public static crypto_hash_sha512_init(param0: native.Array<number>): number;
                public static crypto_scalarmult_curve25519_base(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_auth_hmacsha512256(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_pwhash_primitive(): native.Array<number>;
                public static crypto_generichash_blake2b_keybytes_min(): number;
                public static crypto_generichash_bytes_min(): number;
                public static crypto_box_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_secretbox_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_hash_sha512_bytes(): number;
                public static crypto_auth_bytes(): number;
                public static crypto_sign_ed25519_sk_to_seed(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_pwhash_bytes_max(): number;
                public static crypto_sign_publickeybytes(): number;
                public static crypto_sign_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_pwhash_str_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_generichash_init(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number): number;
                public static crypto_core_hsalsa20_constbytes(): number;
                public static crypto_auth_hmacsha512_init(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_hash_sha512(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static sodium_increment(param0: native.Array<number>, param1: number): void;
                public static crypto_pwhash_saltbytes(): number;
                public static crypto_generichash_primitive(): native.Array<number>;
                public static crypto_sign_primitive(): native.Array<number>;
                public static crypto_generichash_blake2b_init(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number): number;
                public static crypto_sign_ed25519_seedbytes(): number;
                public static crypto_auth_hmacsha512(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_generichash(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number): number;
                public static crypto_pwhash_passwd_min(): number;
                public static crypto_pwhash_memlimit_sensitive(): number;
                public static crypto_generichash_bytes(): number;
                public static crypto_box_publickeybytes(): number;
                public static crypto_aead_chacha20poly1305_ietf_encrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: native.Array<number>, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_core_salsa20(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>): number;
                public static randombytes_stir(): void;
                public static crypto_core_salsa20_outputbytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_publickeybytes(): number;
                public static crypto_stream_xsalsa20(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_generichash_statebytes(): number;
                public static crypto_generichash_blake2b_bytes(): number;
                public static crypto_auth_keybytes(): number;
                public static crypto_core_salsa20_inputbytes(): number;
                public static crypto_pwhash_opslimit_sensitive(): number;
                public static crypto_box_sealbytes(): number;
                public static crypto_auth_primitive(): native.Array<number>;
                public static crypto_onetimeauth(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_generichash_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_hash_sha512_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_sign_ed25519(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_hash_sha256_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_auth_hmacsha256_init(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_box_open_easy_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_zerobytes(): number;
                public static crypto_generichash_blake2b_final(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_seedbytes(): number;
                public static crypto_pwhash_memlimit_interactive(): number;
                public static crypto_box_secretkeybytes(): number;
                public static crypto_box_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>, param6: native.Array<number>): number;
                public static crypto_core_hsalsa20_keybytes(): number;
                public static crypto_auth_hmacsha512256_statebytes(): number;
                public static crypto_onetimeauth_poly1305_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_macbytes(): number;
                public static crypto_pwhash(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: number, param7: number): number;
                public static crypto_core_salsa20_keybytes(): number;
                public static crypto_sign_ed25519_secretkeybytes(): number;
                public static crypto_stream_salsa20_noncebytes(): number;
                public static crypto_scalarmult_curve25519_bytes(): number;
                public static crypto_box_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_open_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_stream_xsalsa20_keybytes(): number;
                public static crypto_onetimeauth_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_sign_secretkeybytes(): number;
                public static randombytes_random(): number;
                public static randombytes_close(): number;
                public static crypto_sign_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_noncebytes(): number;
                public static crypto_generichash_blake2b_saltbytes(): number;
                public static crypto_aead_chacha20poly1305_nsecbytes(): number;
                public static crypto_sign_bytes(): number;
                public static crypto_box_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_sign_ed25519_bytes(): number;
                public static crypto_core_hsalsa20(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_box_zerobytes(): number;
                public static crypto_auth_hmacsha512256_keybytes(): number;
                public static crypto_box_seal(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_primitive(): native.Array<number>;
                public static crypto_core_hsalsa20_inputbytes(): number;
                public static crypto_pwhash_opslimit_interactive(): number;
                public static crypto_box_beforenmbytes(): number;
                public static crypto_stream_chacha20_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_generichash_blake2b_salt_personal(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: native.Array<number>, param7: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_onetimeauth_init(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_boxzerobytes(): number;
                public static crypto_box(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_auth_hmacsha256_bytes(): number;
                public static crypto_onetimeauth_poly1305_init(param0: native.Array<number>, param1: native.Array<number>): number;
                public static sodium_init(): number;
                public static crypto_generichash_blake2b_bytes_min(): number;
                public static crypto_secretbox_noncebytes(): number;
                public static crypto_onetimeauth_bytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_shorthash_bytes(): number;
                public static crypto_generichash_blake2b_keybytes_max(): number;
                public static crypto_secretbox_xsalsa20poly1305_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha256_keybytes(): number;
                public static crypto_aead_chacha20poly1305_keybytes(): number;
                public static crypto_secretbox_open_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_box_seedbytes(): number;
                public static crypto_core_hsalsa20_outputbytes(): number;
                public static sodium_version_string(): native.Array<number>;
                public static crypto_box_curve25519xsalsa20poly1305_secretkeybytes(): number;
                public static crypto_secretbox_boxzerobytes(): number;
                public static crypto_aead_chacha20poly1305_encrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: native.Array<number>, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_hash_sha256_statebytes(): number;
                public static crypto_stream_chacha20(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_auth_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_onetimeauth_poly1305_bytes(): number;
                public static crypto_onetimeauth_poly1305_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_scalarmult_base(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_auth_hmacsha256_statebytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_scalarmult_curve25519_scalarbytes(): number;
                public static crypto_scalarmult_scalarbytes(): number;
                public static crypto_onetimeauth_poly1305_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_ed25519_sk_to_pk(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_noncebytes(): number;
                public static crypto_auth_hmacsha512256_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_secretbox_primitive(): native.Array<number>;
                public static crypto_auth_hmacsha512_bytes(): number;
                public static crypto_sign_ed25519_verify_detached(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_scalarmult_curve25519(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public constructor();
                public static crypto_sign_ed25519_pk_to_curve25519(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_hash_sha256(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_aead_chacha20poly1305_ietf_npubbytes(): number;
                public static crypto_pwhash_memlimit_max(): number;
                public static crypto_aead_chacha20poly1305_ietf_decrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>, param4: number, param5: native.Array<number>, param6: number, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_shorthash_siphash24_bytes(): number;
                public static crypto_auth_hmacsha512_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_open(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha256_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_auth_hmacsha512256_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_verify_detached(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_auth_hmacsha512_statebytes(): number;
                public static crypto_sign_ed25519_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_hash_sha256_init(param0: native.Array<number>): number;
                public static crypto_stream_chacha20_noncebytes(): number;
                public static crypto_onetimeauth_poly1305_keybytes(): number;
                public static crypto_shorthash(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_seedbytes(): number;
                public static crypto_stream_chacha20_ietf_noncebytes(): number;
                public static crypto_core_salsa20_constbytes(): number;
                public static crypto_hash_sha512_statebytes(): number;
                public static crypto_pwhash_memlimit_moderate(): number;
                public static randombytes_uniform(param0: number): number;
                public static crypto_secretbox_keybytes(): number;
                public static crypto_box_beforenm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_generichash_keybytes_max(): number;
                public static crypto_auth_hmacsha512_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_aead_chacha20poly1305_decrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>, param4: number, param5: native.Array<number>, param6: number, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_generichash_blake2b_init_salt_personal(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_shorthash_keybytes(): number;
                public static crypto_box_open_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>, param6: native.Array<number>): number;
                public static crypto_sign_ed25519_sk_to_curve25519(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_generichash_blake2b(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number): number;
                public static crypto_box_easy_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_pwhash_alg_default(): number;
                public static crypto_sign_ed25519_publickeybytes(): number;
                public static crypto_stream_chacha20_keybytes(): number;
                public static crypto_auth_hmacsha512256_init(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_auth_hmacsha512256_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_hash_sha512_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_pwhash_alg_argon2i13(): number;
                public static crypto_stream_chacha20_ietf_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_sign(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_stream_salsa20_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_pwhash_opslimit_max(): number;
                public static crypto_scalarmult_primitive(): native.Array<number>;
                public static crypto_auth_hmacsha256_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_shorthash_siphash24(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha512_keybytes(): number;
                public static crypto_auth(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static randombytes_buf(param0: native.Array<number>, param1: number): void;
                public static crypto_aead_chacha20poly1305_abytes(): number;
            }
        }
    }
}

declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export abstract class SodiumConstants {
                public static SHA256BYTES: number;
                public static SHA512BYTES: number;
                public static BLAKE2B_OUTBYTES: number;
                public static PUBLICKEY_BYTES: number;
                public static SECRETKEY_BYTES: number;
                public static NONCE_BYTES: number;
                public static ZERO_BYTES: number;
                public static BOXZERO_BYTES: number;
                public static SCALAR_BYTES: number;
                public static XSALSA20_POLY1305_SECRETBOX_KEYBYTES: number;
                public static XSALSA20_POLY1305_SECRETBOX_NONCEBYTES: number;
                public static SIGNATURE_BYTES: number;
                public static AEAD_CHACHA20_POLY1305_KEYBYTES: number;
                public static AEAD_CHACHA20_POLY1305_NPUBBYTES: number;
                public static AEAD_CHACHA20_POLY1305_ABYTES: number;
                public constructor();
            }
        }
    }
}

declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export class SodiumJNI {
                public static crypto_generichash_blake2b_bytes_max(): number;
                public static randombytes(param0: native.Array<number>, param1: number): void;
                public static crypto_pwhash_opslimit_moderate(): number;
                public static crypto_secretbox_zerobytes(): number;
                public static crypto_hash_sha256_bytes(): number;
                public static crypto_secretbox_open_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_box_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_keybytes(): number;
                public static crypto_box_detached_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_auth_hmacsha256(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_auth_hmacsha512_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_stream_chacha20_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha512256_bytes(): number;
                public static crypto_generichash_keybytes(): number;
                public static crypto_generichash_blake2b_keybytes(): number;
                public static crypto_generichash_final(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_ed25519_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_sign_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_onetimeauth_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_generichash_blake2b_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_onetimeauth_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_pwhash_passwd_max(): number;
                public static crypto_pwhash_str(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number, param4: number): number;
                public static crypto_stream_xsalsa20_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_generichash_keybytes_min(): number;
                public static crypto_shorthash_siphash24_keybytes(): number;
                public static crypto_secretbox_macbytes(): number;
                public static crypto_secretbox_xsalsa20poly1305_noncebytes(): number;
                public static crypto_onetimeauth_primitive(): native.Array<number>;
                public static crypto_stream_salsa20_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_generichash_bytes_max(): number;
                public static crypto_auth_hmacsha256_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_boxzerobytes(): number;
                public static crypto_sign_ed25519_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_scalarmult_bytes(): number;
                public static crypto_scalarmult(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_box_open_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_pwhash_bytes_min(): number;
                public static crypto_onetimeauth_keybytes(): number;
                public static crypto_generichash_blake2b_personalbytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_open_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_pwhash_strbytes(): number;
                public static crypto_stream_xsalsa20_noncebytes(): number;
                public static crypto_box_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_macbytes(): number;
                public static crypto_pwhash_memlimit_min(): number;
                public static crypto_box_curve25519xsalsa20poly1305_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_stream_chacha20_ietf(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_shorthash_primitive(): native.Array<number>;
                public static crypto_pwhash_opslimit_min(): number;
                public static crypto_stream_salsa20(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_aead_chacha20poly1305_npubbytes(): number;
                public static crypto_onetimeauth_poly1305(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_seal_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_sign_ed25519_open(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_hash_sha256_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_secretbox_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_stream_salsa20_keybytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_stream_xsalsa20_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_boxzerobytes(): number;
                public static crypto_stream_chacha20_ietf_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_beforenm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_macbytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_zerobytes(): number;
                public static crypto_box_open_detached_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_pwhash_strprefix(): native.Array<number>;
                public static crypto_onetimeauth_statebytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_beforenmbytes(): number;
                public static crypto_hash_sha512_init(param0: native.Array<number>): number;
                public static crypto_scalarmult_curve25519_base(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_auth_hmacsha512256(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_pwhash_primitive(): native.Array<number>;
                public static crypto_generichash_blake2b_keybytes_min(): number;
                public static crypto_generichash_bytes_min(): number;
                public static crypto_box_easy(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_secretbox_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_hash_sha512_bytes(): number;
                public static crypto_auth_bytes(): number;
                public static crypto_sign_ed25519_sk_to_seed(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_pwhash_bytes_max(): number;
                public static crypto_sign_publickeybytes(): number;
                public static crypto_sign_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_pwhash_str_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_generichash_init(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number): number;
                public static crypto_core_hsalsa20_constbytes(): number;
                public static crypto_auth_hmacsha512_init(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_hash_sha512(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static sodium_increment(param0: native.Array<number>, param1: number): void;
                public static crypto_pwhash_saltbytes(): number;
                public static crypto_generichash_primitive(): native.Array<number>;
                public static crypto_sign_primitive(): native.Array<number>;
                public static crypto_generichash_blake2b_init(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number): number;
                public static crypto_sign_ed25519_seedbytes(): number;
                public static crypto_auth_hmacsha512(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_generichash(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number): number;
                public static crypto_pwhash_passwd_min(): number;
                public static crypto_pwhash_memlimit_sensitive(): number;
                public static crypto_generichash_bytes(): number;
                public static crypto_box_publickeybytes(): number;
                public static crypto_aead_chacha20poly1305_ietf_encrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: native.Array<number>, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_core_salsa20(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>): number;
                public static randombytes_stir(): void;
                public static crypto_core_salsa20_outputbytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_publickeybytes(): number;
                public static crypto_stream_xsalsa20(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_generichash_statebytes(): number;
                public static crypto_generichash_blake2b_bytes(): number;
                public static crypto_auth_keybytes(): number;
                public static crypto_core_salsa20_inputbytes(): number;
                public static crypto_pwhash_opslimit_sensitive(): number;
                public static crypto_box_sealbytes(): number;
                public static crypto_auth_primitive(): native.Array<number>;
                public static crypto_onetimeauth(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_generichash_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_hash_sha512_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_sign_ed25519(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_hash_sha256_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_auth_hmacsha256_init(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_box_open_easy_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305_zerobytes(): number;
                public static crypto_generichash_blake2b_final(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_seedbytes(): number;
                public static crypto_pwhash_memlimit_interactive(): number;
                public static crypto_box_secretkeybytes(): number;
                public static crypto_box_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>, param6: native.Array<number>): number;
                public static crypto_core_hsalsa20_keybytes(): number;
                public static crypto_auth_hmacsha512256_statebytes(): number;
                public static crypto_onetimeauth_poly1305_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_macbytes(): number;
                public static crypto_pwhash(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: number, param7: number): number;
                public static crypto_core_salsa20_keybytes(): number;
                public static crypto_sign_ed25519_secretkeybytes(): number;
                public static crypto_stream_salsa20_noncebytes(): number;
                public static crypto_scalarmult_curve25519_bytes(): number;
                public static crypto_box_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_open_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_stream_xsalsa20_keybytes(): number;
                public static crypto_onetimeauth_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_sign_secretkeybytes(): number;
                public static randombytes_random(): number;
                public static randombytes_close(): number;
                public static crypto_sign_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_noncebytes(): number;
                public static crypto_generichash_blake2b_saltbytes(): number;
                public static crypto_aead_chacha20poly1305_nsecbytes(): number;
                public static crypto_sign_bytes(): number;
                public static crypto_box_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_sign_ed25519_bytes(): number;
                public static crypto_core_hsalsa20(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_box_zerobytes(): number;
                public static crypto_auth_hmacsha512256_keybytes(): number;
                public static crypto_box_seal(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_primitive(): native.Array<number>;
                public static crypto_core_hsalsa20_inputbytes(): number;
                public static crypto_pwhash_opslimit_interactive(): number;
                public static crypto_box_beforenmbytes(): number;
                public static crypto_stream_chacha20_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_generichash_blake2b_salt_personal(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: native.Array<number>, param7: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_onetimeauth_init(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_boxzerobytes(): number;
                public static crypto_box(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_auth_hmacsha256_bytes(): number;
                public static crypto_onetimeauth_poly1305_init(param0: native.Array<number>, param1: native.Array<number>): number;
                public static sodium_init(): number;
                public static crypto_generichash_blake2b_bytes_min(): number;
                public static crypto_secretbox_noncebytes(): number;
                public static crypto_onetimeauth_bytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_seed_keypair(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_shorthash_bytes(): number;
                public static crypto_generichash_blake2b_keybytes_max(): number;
                public static crypto_secretbox_xsalsa20poly1305_open(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha256_keybytes(): number;
                public static crypto_aead_chacha20poly1305_keybytes(): number;
                public static crypto_secretbox_open_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_box_seedbytes(): number;
                public static crypto_core_hsalsa20_outputbytes(): number;
                public static sodium_version_string(): native.Array<number>;
                public static crypto_box_curve25519xsalsa20poly1305_secretkeybytes(): number;
                public static crypto_secretbox_boxzerobytes(): number;
                public static crypto_aead_chacha20poly1305_encrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number, param6: native.Array<number>, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_hash_sha256_statebytes(): number;
                public static crypto_stream_chacha20(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: native.Array<number>): number;
                public static crypto_auth_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_onetimeauth_poly1305_bytes(): number;
                public static crypto_onetimeauth_poly1305_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_scalarmult_base(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_auth_hmacsha256_statebytes(): number;
                public static crypto_box_curve25519xsalsa20poly1305_keypair(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_scalarmult_curve25519_scalarbytes(): number;
                public static crypto_scalarmult_scalarbytes(): number;
                public static crypto_onetimeauth_poly1305_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_ed25519_sk_to_pk(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_box_noncebytes(): number;
                public static crypto_auth_hmacsha512256_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_secretbox_primitive(): native.Array<number>;
                public static crypto_auth_hmacsha512_bytes(): number;
                public static crypto_sign_ed25519_verify_detached(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_scalarmult_curve25519(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public constructor();
                public static crypto_sign_ed25519_pk_to_curve25519(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_hash_sha256(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_aead_chacha20poly1305_ietf_npubbytes(): number;
                public static crypto_pwhash_memlimit_max(): number;
                public static crypto_aead_chacha20poly1305_ietf_decrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>, param4: number, param5: native.Array<number>, param6: number, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_shorthash_siphash24_bytes(): number;
                public static crypto_auth_hmacsha512_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_open(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha256_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_auth_hmacsha512256_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_sign_verify_detached(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_auth_hmacsha512_statebytes(): number;
                public static crypto_sign_ed25519_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_hash_sha256_init(param0: native.Array<number>): number;
                public static crypto_stream_chacha20_noncebytes(): number;
                public static crypto_onetimeauth_poly1305_keybytes(): number;
                public static crypto_shorthash(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_box_curve25519xsalsa20poly1305_seedbytes(): number;
                public static crypto_stream_chacha20_ietf_noncebytes(): number;
                public static crypto_core_salsa20_constbytes(): number;
                public static crypto_hash_sha512_statebytes(): number;
                public static crypto_pwhash_memlimit_moderate(): number;
                public static randombytes_uniform(param0: number): number;
                public static crypto_secretbox_keybytes(): number;
                public static crypto_box_beforenm(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>): number;
                public static crypto_generichash_keybytes_max(): number;
                public static crypto_auth_hmacsha512_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_aead_chacha20poly1305_decrypt(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: native.Array<number>, param4: number, param5: native.Array<number>, param6: number, param7: native.Array<number>, param8: native.Array<number>): number;
                public static crypto_generichash_blake2b_init_salt_personal(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: number, param4: native.Array<number>, param5: native.Array<number>): number;
                public static crypto_shorthash_keybytes(): number;
                public static crypto_box_open_detached(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: native.Array<number>, param6: native.Array<number>): number;
                public static crypto_sign_ed25519_sk_to_curve25519(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_generichash_blake2b(param0: native.Array<number>, param1: number, param2: native.Array<number>, param3: number, param4: native.Array<number>, param5: number): number;
                public static crypto_box_easy_afternm(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_pwhash_alg_default(): number;
                public static crypto_sign_ed25519_publickeybytes(): number;
                public static crypto_stream_chacha20_keybytes(): number;
                public static crypto_auth_hmacsha512256_init(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_auth_hmacsha512256_final(param0: native.Array<number>, param1: native.Array<number>): number;
                public static crypto_hash_sha512_update(param0: native.Array<number>, param1: native.Array<number>, param2: number): number;
                public static crypto_pwhash_alg_argon2i13(): number;
                public static crypto_stream_chacha20_ietf_xor(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_sign(param0: native.Array<number>, param1: native.Array<number>, param2: native.Array<number>, param3: number, param4: native.Array<number>): number;
                public static crypto_stream_salsa20_xor_ic(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: number, param5: native.Array<number>): number;
                public static crypto_pwhash_opslimit_max(): number;
                public static crypto_scalarmult_primitive(): native.Array<number>;
                public static crypto_auth_hmacsha256_verify(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_shorthash_siphash24(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static crypto_secretbox_xsalsa20poly1305(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>, param4: native.Array<number>): number;
                public static crypto_auth_hmacsha512_keybytes(): number;
                public static crypto_auth(param0: native.Array<number>, param1: native.Array<number>, param2: number, param3: native.Array<number>): number;
                public static randombytes_buf(param0: native.Array<number>, param1: number): void;
                public static crypto_aead_chacha20poly1305_abytes(): number;
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Encoder.d.ts" />
/// <reference path="./org.libsodium.jni.keys.PrivateKey.d.ts" />
/// <reference path="./org.libsodium.jni.keys.PublicKey.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace crypto {
                export class Box {
                    public constructor(param0: org.libsodium.jni.keys.PublicKey, param1: org.libsodium.jni.keys.PrivateKey);
                    public decrypt(param0: native.Array<number>, param1: native.Array<number>): native.Array<number>;
                    public decrypt(param0: string, param1: string, param2: org.libsodium.jni.encoders.Encoder): native.Array<number>;
                    public encrypt(param0: native.Array<number>, param1: native.Array<number>): native.Array<number>;
                    public constructor(param0: native.Array<number>, param1: native.Array<number>);
                    public encrypt(param0: string, param1: string, param2: org.libsodium.jni.encoders.Encoder): native.Array<number>;
                    public constructor(param0: string, param1: string, param2: org.libsodium.jni.encoders.Encoder);
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Encoder.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace crypto {
                export class Hash {
                    public sha512(param0: native.Array<number>): native.Array<number>;
                    public blake2(param0: string, param1: org.libsodium.jni.encoders.Encoder): string;
                    public blake2(param0: native.Array<number>): native.Array<number>;
                    public constructor();
                    public sha256(param0: native.Array<number>): native.Array<number>;
                    public sha512(param0: string, param1: org.libsodium.jni.encoders.Encoder): string;
                    public sha256(param0: string, param1: org.libsodium.jni.encoders.Encoder): string;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Encoder.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace crypto {
                export class Point {
                    public toString(): string;
                    public constructor(param0: native.Array<number>);
                    public mult(param0: string, param1: org.libsodium.jni.encoders.Encoder): org.libsodium.jni.crypto.Point;
                    public constructor();
                    public mult(param0: native.Array<number>): org.libsodium.jni.crypto.Point;
                    public constructor(param0: string, param1: org.libsodium.jni.encoders.Encoder);
                    public toBytes(): native.Array<number>;
                }
            }
        }
    }
}

declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace crypto {
                export class Random {
                    public constructor();
                    public randomBytes(): native.Array<number>;
                    public randomBytes(param0: number): native.Array<number>;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Encoder.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace crypto {
                export class SecretBox {
                    public constructor(param0: native.Array<number>);
                    public decrypt(param0: native.Array<number>, param1: native.Array<number>): native.Array<number>;
                    public encrypt(param0: native.Array<number>, param1: native.Array<number>): native.Array<number>;
                    public constructor(param0: string, param1: org.libsodium.jni.encoders.Encoder);
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace crypto {
                export class Util {
                    public static merge(param0: native.Array<number>, param1: native.Array<number>): native.Array<number>;
                    public constructor();
                    public static prependZeros(param0: number, param1: native.Array<number>): native.Array<number>;
                    public static slice(param0: native.Array<number>, param1: number, param2: number): native.Array<number>;
                    public static removeZeros(param0: number, param1: native.Array<number>): native.Array<number>;
                    public static checkLength(param0: native.Array<number>, param1: number): void;
                    public static zeros(param0: number): native.Array<number>;
                    public static isValid(param0: number, param1: string): boolean;
                }
            }
        }
    }
}

import javaniocharsetCharset = java.nio.charset.Charset;
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.nio.charset.Charset.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Hex.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Raw.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace encoders {
                export class Encoder {
                    /**
                     * Constructs a new instance of the org.libsodium.jni.encoders.Encoder interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        decode(param0: string): native.Array<number>;
                        encode(param0: native.Array<number>): string;
                        <clinit>(): void;
                    });
                    public static RAW: org.libsodium.jni.encoders.Raw;
                    public static CHARSET: javaniocharsetCharset;
                    public static HEX: org.libsodium.jni.encoders.Hex;
                    public encode(param0: native.Array<number>): string;
                    public decode(param0: string): native.Array<number>;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace encoders {
                export class Hex {
                    public toString(): string;
                    public constructor();
                    public encode(param0: native.Array<number>): string;
                    public decode(param0: string): native.Array<number>;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace encoders {
                export class Raw {
                    public constructor();
                    public encode(param0: native.Array<number>): string;
                    public decode(param0: string): native.Array<number>;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Encoder.d.ts" />
/// <reference path="./org.libsodium.jni.keys.PrivateKey.d.ts" />
/// <reference path="./org.libsodium.jni.keys.PublicKey.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace keys {
                export class KeyPair {
                    public constructor(param0: native.Array<number>);
                    public getPrivateKey(): org.libsodium.jni.keys.PrivateKey;
                    public constructor();
                    public getPublicKey(): org.libsodium.jni.keys.PublicKey;
                    public constructor(param0: string, param1: org.libsodium.jni.encoders.Encoder);
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace keys {
                export class PrivateKey {
                    public toString(): string;
                    public constructor(param0: native.Array<number>);
                    public constructor(param0: string);
                    public toBytes(): native.Array<number>;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace keys {
                export class PublicKey {
                    public toString(): string;
                    public constructor(param0: native.Array<number>);
                    public constructor(param0: string);
                    public toBytes(): native.Array<number>;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Encoder.d.ts" />
/// <reference path="./org.libsodium.jni.keys.VerifyKey.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace keys {
                export class SigningKey {
                    public sign(param0: native.Array<number>): native.Array<number>;
                    public toString(): string;
                    public constructor(param0: native.Array<number>);
                    public constructor();
                    public getVerifyKey(): org.libsodium.jni.keys.VerifyKey;
                    public sign(param0: string, param1: org.libsodium.jni.encoders.Encoder): string;
                    public constructor(param0: string, param1: org.libsodium.jni.encoders.Encoder);
                    public toBytes(): native.Array<number>;
                }
            }
        }
    }
}

/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./org.libsodium.jni.encoders.Encoder.d.ts" />
declare namespace org {
    export namespace libsodium {
        export namespace jni {
            export namespace keys {
                export class VerifyKey {
                    public toString(): string;
                    public constructor(param0: native.Array<number>);
                    public verify(param0: native.Array<number>, param1: native.Array<number>): boolean;
                    public verify(param0: string, param1: string, param2: org.libsodium.jni.encoders.Encoder): boolean;
                    public constructor(param0: string, param1: org.libsodium.jni.encoders.Encoder);
                    public toBytes(): native.Array<number>;
                }
            }
        }
    }
}

