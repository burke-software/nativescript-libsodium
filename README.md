# Nativescript libsodium

Use native libsodium with nativescript. (mostly) API compatible with libsodium.js wrappers.

Only Android is supported at this time.

## Installation

`tns plugin add nativescript-libsodium`

For Android - libsodium-jni requires either disallowing backup or making the following changes to your AndroidManifest.xml

*Don't use backup if you are storing secret keys as they would also get backed up*

- Add `tools:replace="android:allowBackup"` as an attribute to the application tag.
- Add `xmlns:tools="http://schemas.android.com/tools"` asn an attribute to the manifest tag.

## Usage 

Use it the same way you use [libsodium.js](https://github.com/jedisct1/libsodium.js) including their wrapper functions. 

```
import { Libsodium } from "nativescript-libsodium";
let keypair = Libsodium.crypto_box_keypair();
console.log(keypair.privateKey);  // It's a Uint8Array object
```

For more examples, check out the [unit tests](/demo/app/tests/tests.ts)

## Contributing

Not all libsodium functions are wrapped in this project. But they are easy to add. 
Let's see how to add the crypto_box_seal_open function.

### Getting familiar

1. First run the project as is. You may run both the demo and unit tests. Read instructions [here](https://github.com/NativeScript/nativescript-plugin-seed)
2. Review docs on the function from [libsodium](https://download.libsodium.org/doc/public-key_cryptography/sealed_boxes.html) and libsodium.js
3. Review [type definitions for libsodium.js](https://gitlab.com/burke-software/passit/simple-asymmetric-js/blob/master/simple_asym/libsodium.d.ts) and [libsodium-jni](/src/libsodium-jni.d.ts)

### Adding new functions

- Add your new function (this one already exists actually) to [libsodium.common.ts](/src/libsodium.common.ts)
- Write function parameters as to match libsodium.js's wrapper. 
- Convert Uint8Array to Native Byte arrays with force_java_bytes
- Run native function. Review libsodium docs (not .js)
- Convert back to Uint8Array with javvaByteArrayToUint8Array
- Add unit test demonstrating functionality.
- Submit your merge request :)

```
  public static crypto_box_seal(message: string | Uint8Array, publicKey: Uint8Array): Uint8Array {
    let messageBytes = this.force_java_bytes(message);
    let publicKeyBytes = this.force_java_bytes(publicKey);
    let ciphertext: JavaBytes = Array.create('byte', Sodium.crypto_box_sealbytes() + messageBytes.length);

    Sodium.crypto_box_seal(ciphertext, messageBytes, messageBytes.length, publicKeyBytes);

    return this.javaByteArrayToUint8Array(ciphertext);
  }
```

## Design decisions

- We wish to maintain as close as possible API compatibility with libsodium.js wrappers. This means converting between JS Uint8Array and Java Byte Arrays. There is some performance penally for this but it allows us to "drop in" nativescript-libsodium in some projects that use libsodium.js.
- libsodium-jni is not compiled from source but from a build server. You may wish to build it yourself.
- We do not support libsodium's outputFormat param which complicates type definitions. This could change especially if someone submitted a merge request.
- Error handling could be improved - it should take the native function's return value and throw an exception if it's unable to perform the crypto. 

## iOS Support?

Please contribute! You would need to refactor the android specific code to .android files and build a wrapper for the .ios ones. 

It might make sense to make a generic class interface and have both Android and iOS implement it to ensure consistency.

## Need paid support?

Do you need an iOS version sooner? Or maybe prefer us to add more libsodium functions that are important to you?

Let us know at info AT burke software dot com.
    
## License

Apache License Version 2.0, January 2004
